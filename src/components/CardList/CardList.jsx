import React, { useState } from 'react'
import { CharacterCard } from '../CharacterCard/CharacterCard';
import { useCharacters } from '../../hooks/useCharacters';
import './CardList.css';
import { SearchCharacter } from '../SearchCharacter/SearchCharacter';
import NotCharacterFound from '../NotCharacterFound/NotCharacterFound';
import SelectPage from '../SelectPage/SelectPage';
import { Link } from 'react-router-dom';

function CardList() {

    const [endpoint, setEndpoint] = useState('https://rickandmortyapi.com/api/character');
    const {data, isLoading, error} = useCharacters(endpoint);
    const [charactersList, setCharactersList] = useState([]);
    const selectedStyles = {
        divTrue: 'card-container on-effect',
        divFalse: 'card-container',
        article: 'character-card',
        firstSectionTrue: 'moreInfo',
        firstSectionFalse: 'moreInfo hidden',
        secondSectionTrue: 'basicInfo hidden',
        secondSectionFalse: 'basicInfo',
        sectionDivTrue: 'hidden',
        sectionDivFalse: undefined,
        img: 'img-character'
    }

    if(isLoading) return <p>Cargando...</p>;

    const {results} = data;
    const {info} = data;

    return (
        <section className="characterList" id="characterList">
            <SearchCharacter allCharacters={results} isLoading={isLoading} error={error} setCharactersList={setCharactersList}/>
            <Link to={'/global-filter'}>
                <button className='find-character'>...or click here and find a character among all pages</button>
            </Link>
            <SelectPage setEndpoint={setEndpoint} info={info}/>
            <ul className="cardList">
                {
                    charactersList?.length ?
                    charactersList.map((characterInfo)=>{
                        return <li key={characterInfo.id}><CharacterCard characterInfo={characterInfo} selectedStyles={selectedStyles}/></li>
                    })
                    :
                    <NotCharacterFound/>
                }
            </ul>
            {
                error && <p>{error}</p>
            }
        </section>
        
    )
}

export {CardList};