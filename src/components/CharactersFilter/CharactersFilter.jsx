import React, { useEffect, useState } from 'react';
import FormFilter from '../FormFilter.jsx/FormFilter';
import { CharacterCard } from '../CharacterCard/CharacterCard';
import './CharactersFilter.css'
import { useCharacters } from '../../hooks/useCharacters';

function CharactersFilter() {
  const [endpoint, setEndpoint] = useState('https://rickandmortyapi.com/api/character');
  const {data, isLoading, error} = useCharacters(endpoint);

  const selectedStyles = {
    divTrue: 'filtered-character-card-container on-effect',
    divFalse: 'filtered-character-card-container',
    article: 'filtered-character-card',
    firstSectionTrue: 'moreInfoFiltered',
    firstSectionFalse: 'moreInfoFiltered hidden',
    secondSectionTrue: 'basicInfoFiltered hidden',
    secondSectionFalse: 'basicInfoFiltered',
    sectionDivTrue: 'hidden',
    sectionDivFalse: undefined,
    img: 'img-character-filtered'
}

  if(isLoading) return <p>Cargando...</p>

  const {results} = data;
  const {info} = data;
  const {pages : totalPages} = info;

  return (
    <section className='filter-page'>
        <FormFilter totalPages={totalPages} setEndpoint={setEndpoint} isLoading={isLoading} error={error}/>
        <ul className="filtered-card">
          {
              results?.length ?
              results.map((characterInfo)=>{
                  return <li key={characterInfo.id}><CharacterCard characterInfo={characterInfo} selectedStyles={selectedStyles}/></li>
              })
              :
              <p>Un saludito</p>
          }
        </ul>
    </section>
  );
  
}

export default CharactersFilter