import React from 'react';
import './NotCharacterFound.css'
import { Link } from 'react-router-dom';

function NotCharacterFound() {
  return (
    <li key={"not-found-key"}>
      <section className="not-character-found-section">
        <img
          src="/NotCharacterFound.jpg"
          className="not-character-found-img"
        />
        <div className='msg-container'>
          <p className='not-found-message'>Oh, come on<span className='green-txt'>!</span></p>
          <p className='not-found-message'>There are <span className='green-txt'>no characters</span> with that name <span className='green-txt'>on this page</span>...</p>
          <Link to={'/global-filter'}>
            <button className='clear-btn'>Use global filter</button>
          </Link>
        </div>
      </section>
    </li>
  )
}

export default NotCharacterFound;