import React from 'react';
import logoInicio from '/rickAndMorty.png';
import { Link } from 'react-router-dom';

function InitialSection() {
    
    return (
        <section className="initial-section">
            <img src={logoInicio} alt="Rick and Morty" className="img-presentation"></img>
            <div className="presentation-text">
                <p>Hi, I'm Antía!</p>
                <p className="p-presentation">This is a small practice project that I developed using React and Vite.</p>
                <p className="p-presentation">Thanks to the Rick and Morty API, you will be able to access the data of the characters that appear in this animated series.</p>
                <div className='btn-container'>
                    <Link to={'/characters'}>
                        <button className="btn-next-page">Go to the characters</button>
                    </Link>
                </div>
            </div>
        </section>
    )
}

export {InitialSection};