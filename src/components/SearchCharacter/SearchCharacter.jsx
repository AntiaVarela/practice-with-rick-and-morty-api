import React, { useEffect } from 'react';
import './SearchCharacter.css';
import searchIcon from '/lupa.png'

function SearchCharacter({allCharacters, error, setCharactersList}) {

    useEffect (() => {
        setCharactersList(allCharacters);
    },[]);
    
    function handleChange(e){
        const {value} = e.target;

        const findCharacter = allCharacters.filter((character) => character.name.toLowerCase().includes(value.toLowerCase()));

		findCharacter?.length ? setCharactersList(findCharacter) : setCharactersList([]);
        
        !value?.length && setCharactersList(allCharacters);
    }

    

    return (
        <form onSubmit={(e) => e.preventDefault()}>
            <label htmlFor="search-character">
                <input 
                    type="text"
                    name="search-character"
                    id="search-character"
                    placeholder='Search for a character on this page...'
                    onChange={handleChange}
                />
                <img
                    src={searchIcon}
                    className="input-icon"
                />
            </label>
            <label className="error-label">{error && <p>`Oops! We have a problem: ${error.toLowerCase()}`</p>}</label>
        </form>
    )
}

export {SearchCharacter}