import React, { useEffect, useState } from 'react';
import './SelectPage.css';

function SelectPage({setEndpoint, info}) {

  const {prev, next, pages} = info;

  const splitedNext = next?.split("=");
  const splitedPrev = prev?.split("=");

  const baseEndpoint = next && splitedNext[0] || prev && splitedPrev[0];
  const nextPage = next && parseInt(splitedNext[1]);
  const prevPage = prev && parseInt(splitedPrev[1]);

  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    prevPage ? setCurrentPage(prevPage+1) : nextPage && setCurrentPage(nextPage-1);
  }, [prevPage]);

  function handleClick(e){

    const {className : arrowClass} = e.target;
    const {baseVal : arrow} = arrowClass;

    if(arrow.includes('arrow-next')){
      setCurrentPage(currentPage+1);
      setEndpoint(`${baseEndpoint}=${currentPage+1}`);
      
    }else if(arrow.includes('arrow-prev')){
      setCurrentPage(currentPage-1)
      setEndpoint(`${baseEndpoint}=${currentPage-1}`);
    }
    
  }

  return (
    <section>
      <div className='page-btn-container'>
        <button disabled={prevPage === null ? true : false} className="arrow-btn" onClick={handleClick}><svg className="arrow-prev" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path fill="currentColor" d="m10.108 12l4.246 4.246q.14.14.15.344q.01.204-.15.364t-.354.16q-.194 0-.354-.16l-4.388-4.389q-.131-.13-.184-.267q-.053-.136-.053-.298t.053-.298q.053-.137.184-.267l4.388-4.389q.14-.14.344-.15q.204-.01.364.15t.16.354q0 .194-.16.354z"/></svg></button>
        {!nextPage && <button className='select-page-btn' onClick={()=>{setEndpoint(`${baseEndpoint}=${prevPage-1}`)}}>{prevPage-1}</button>}
        {!prev ? <button className={`select-page-btn ${currentPage === 1 && 'current-page'}`} onClick={()=>{setEndpoint('https://rickandmortyapi.com/api/character')}}>1</button> : <button className={`select-page-btn`} onClick={()=>{setEndpoint(`${baseEndpoint}=${prevPage}`)}}>{prevPage}</button>}
        {prev && <button className='select-page-btn current-page'>{prevPage ? prevPage+1 : 2}</button>}
        {nextPage && <button className='select-page-btn' onClick={()=>{setEndpoint(`${baseEndpoint}=${nextPage}`)}}>{nextPage}</button>}
        {!prev && <button className='select-page-btn' onClick={()=>{setEndpoint(`${baseEndpoint}=${nextPage+1}`)}}>{nextPage+1}</button>}
        <button disabled={currentPage === pages ? true : false} className="arrow-btn" onClick={handleClick}><svg className="arrow-next" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path fill="currentColor" d="M13.292 12L9.046 7.754q-.14-.14-.15-.344q-.01-.204.15-.364t.354-.16q.194 0 .354.16l4.388 4.389q.131.13.184.267q.053.136.053.298t-.053.298q-.053.137-.184.267l-4.388 4.389q-.14.14-.344.15q-.204.01-.364-.15t-.16-.354q0-.194.16-.354z"/></svg></button>
      </div>
    </section>
  )
}

export default SelectPage