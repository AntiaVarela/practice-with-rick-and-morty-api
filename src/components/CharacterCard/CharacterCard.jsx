import React, { useState } from 'react';
import './CharacterCard.css';

function CharacterCard({characterInfo, selectedStyles}) {

    const [moreInfo, setMoreInfo] = useState(false);

    const {
        divTrue,
        divFalse,
        article,
        firstSectionTrue,
        firstSectionFalse,
        secondSectionTrue,
        secondSectionFalse,
        sectionDivTrue,
        sectionDivFalse,
        img
    } = selectedStyles;

    return (
        <div className={moreInfo ? divTrue : divFalse} onClick={() => {setMoreInfo(!moreInfo)}}>
            <article className={article} >
                <section className={moreInfo ? firstSectionTrue : firstSectionFalse} onClick={() => {setMoreInfo(!moreInfo)}}>
                    {characterInfo.type ? <p>{characterInfo.type}</p> : <p>{characterInfo.species}</p>}
                    <p>{characterInfo.location.name}</p>
                    <p>{characterInfo.status}</p>
                </section>
                <section className={moreInfo ? secondSectionTrue : secondSectionFalse}>
                    <div className={moreInfo ? sectionDivTrue : sectionDivFalse}>
                        <img src={characterInfo.image} alt={characterInfo.name} className={img}/>
                        <h2>{characterInfo.name}</h2>
                    </div>
                </section>
            </article>
        </div>
    )
}

export {CharacterCard};