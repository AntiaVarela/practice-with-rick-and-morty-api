import React, { useEffect, useState } from 'react'
import { useCharacters } from '../../hooks/useCharacters';
import { deleteDuplicates } from '../../helpers/helpers';
import './FormFilter.css';

function FormFilter({totalPages, setEndpoint}) {
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [species, setSpecies] = useState("");
  const [type, setType] = useState("");
  const [gender, setGender] = useState("");
  const globalCharactersArray = [];
  const allStatusArray = ['Alive', 'Dead', 'Unknown'];
  const allSpeciesArray = [];
  const allTypesArray = [];
  const allGendersArray = ['Female', 'Male', 'Genderless', 'Unknown'];
  let params = {};

//--------------------------------------------------------------------------------------------------
//entiendo que esto no es lo más óptimo del mundo, pero quiero tener todas las especies y los tipos
  for (let i = 1; i < totalPages; i++) {
    const {data} = useCharacters(`https://rickandmortyapi.com/api/character?page=${i}`);
    globalCharactersArray.push(data.results);
  }

  globalCharactersArray?.length &&
    globalCharactersArray?.map((pageArray) => {
      return pageArray?.map((character) => {
        allSpeciesArray.push(character.species);
        allTypesArray.push(character.type);
      });
    })

    deleteDuplicates(allSpeciesArray);
    deleteDuplicates(allTypesArray);
//--------------------------------------------------------------------------------------------------

    function handleChange(e){
      const {name, checked, value} = e.target;

      switch(name){
        case 'name':
          params.name = value;
          setName(value);
          break;

        case 'status':
          allStatusArray.forEach((status) => {
            if((value === status) && checked){
              params.status = value;
              setStatus(value);
            }
          })
          break;

        case 'species':
          allSpeciesArray.forEach((species) => {
            if((value === species) && checked){
              params.species = value;
              setSpecies(value);
            }
          })
          break;

        case 'types':
          allTypesArray.forEach((types) =>{
            if((value === types) && checked){
              params.type = value;
              setType(value);
            }
          })
          break;

        case 'genders':
          allGendersArray.forEach((genders) => {
            if((value === genders) && checked){
              params.gender = value;
              setGender(value);
            }
          })
          break;

        default:
          break;
      }
    }

    function handleSubmit(e){
      e.preventDefault();

      if(name !== "") params.name = name;
      if(status !== "") params.status = status;
      if(species !== "") params.species = species;
      if(type !== "") params.type = type;
      if(gender !== "") params.gender = gender;

      const queryParams = new URLSearchParams(params).toString();

      setEndpoint(`https://rickandmortyapi.com/api/character/?${queryParams}`);
    }

  return (
    <section className='section-form'>
      <form onSubmit={handleSubmit}>
      <button type='submit'>BUSCAR</button>
        <label className='search-name-label'>
          Name
          <input className='search-name-input' type='text' name='name' value={name} placeholder='Write here the name' onChange={handleChange}/>
        </label>
        <fieldset>
          Status
          <ul>
            {
              allStatusArray?.length && 
                allStatusArray.map((status) => 
                  <li key={status}><label>{status} <input className='radio-btn-input' name={'status'} value={status} type='radio' onChange={handleChange}/></label></li>
                )
            }
          </ul>
        </fieldset>
        <fieldset>
          Genders
          <ul>
            {
              allGendersArray?.length && 
              allGendersArray.map((genders) =>
                  <li key={genders}><label>{genders} <input className='radio-btn-input' name={'genders'} value={genders} type='radio' onChange={handleChange}/></label></li>
                )
            }
          </ul>
        </fieldset>
        <fieldset>
          Species
          <ul>
            {
              allSpeciesArray?.length && 
                allSpeciesArray.map((species) => 
                  <li key={species}><label>{species} <input className='radio-btn-input' name={'species'} value={species} type='radio' onChange={handleChange}/></label></li>
                )
            }
          </ul>
        </fieldset>
        <fieldset>
          Types
          <ul>
            {
              allTypesArray?.length && 
              allTypesArray.map((types) =>
                  <li key={types}><label>{types} <input className='radio-btn-input' name={'types'} value={types} type='radio' onChange={handleChange}/></label></li>
                )
            }
          </ul>
        </fieldset>
      </form>
    </section>
  )
}

export default FormFilter