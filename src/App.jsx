import { useState } from 'react';
import { CardList } from './components/CardList/CardList';
import CharactersFilter from './components/CharactersFilter/CharactersFilter';
import { InitialSection } from './components/InitialSection/InitialSection';
import { useCharacters } from './hooks/useCharacters';
import './index.css';
import { Link, Route, Routes} from 'react-router-dom';


function App() {

  return (
    <>
      <header>
        <div className="bg-1">
          <Link to={'/'}><button className="h1-btn"><h1>Rick and Morty</h1></button></Link>
        </div>
      </header>
      <main>
        <Routes>
          <Route path='/' element={<InitialSection/>}/>
          <Route path='/characters' element={<CardList/>}/>
          <Route path='/global-filter' element={<CharactersFilter/>}/>
        </Routes>
      </main>
    </>
  )
}

export default App;
