# React + Vite + Rick And Morty API

This is a practice exercise done with React, Vite and the Rick and Morty API.
As I progress in the development of the website, I will update the readme with the possibilities offered by this small project.

Thanks for your time!